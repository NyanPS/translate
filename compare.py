#!/usr/bin/env python3

import convert
import sys

NOTRANS = 0
TRANS = 1

def pc(c):
    print(c, end="")

def read_file(file):
    attributes = set()
    with open(file, "r", encoding="utf-8") as f:
        key = ""
        noKeyInNextLine = False
        for l in f:
            if noKeyInNextLine:
                isKey = False
                noKeyInNextLine = False
            else:
                key = ""
                isKey = True
            checkNext = False
            for i, c in enumerate(l):
                if checkNext and not isKey:
                    if c == "\n":
                        noKeyInNextLine = True
                    checkNext = False
                elif c == "=":
                    isKey = False
                elif c == "\\":
                    checkNext = True
                elif c == "\n":
                    pass
                else:
                    if isKey:
                        key += c
            if noKeyInNextLine:
                pass
            else:
                attributes.add(key)
    return attributes

a = read_file(sys.argv[1])
b = read_file(sys.argv[2])
d = set()
for c in a:
    if c in b:
        b.remove(c)
    else:
        d.add(c)


for c in d:
    print("---" + c)
for c in b:
    print("+++" + c)
