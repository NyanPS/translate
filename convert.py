#!/usr/bin/env python3

import sys

def char_set(upper = 65, lower = 97, digit = 48, special = {}):
    if lower == 26:
        lower = upper + 26

    def func(c):
        try:
            a = special[c]
            if isinstance(a, int):
                return chr(a)
            elif isinstance(a, str):
                return a
        except KeyError:
            if c.isupper():
                return chr(ord(c) + upper - 65)
            elif c.islower() and upper != 65 and lower == 97:
                return chr(ord(c) + upper - 97)
            elif c.islower():
                return chr(ord(c) + lower - 97)
            elif c.isdigit():
                return chr(ord(c) + digit - 48)
            else:
                return c
    return func

def caps_special_char_set(special):
    def func(c):
        c = c.upper()
        try:
            a = special[c]
            if isinstance(a, int):
                return chr(a)
            elif isinstance(a, str):
                return a
        except KeyError:
            return c
    return func

def print_help():
    quit()

def print_sets():
    for k in types.keys():
        print(k)

def h(string):
    return int(string, 16)

special_circled = {"0": "\u24EA",
                "*": "\u229B",
                "/": "\u2298",
                "+": "\u2295",
                ">": "\u29C1",
                "<": "\u29C0",
                "=": "\u229C",
                "-": "\u229D",
                "\\": "\u29B8",
                "|": "\u29B6",
        }

special_runic = {
                "A":"\uA2EB",
                "B":"\uA0C3",
                "C":"\uA3F8",
                "D":"\uA055",
                "E":"\uA35F",
                "F":"\uA118",
                "G":"\uA04D",
                "H":"\uA45B",
                "I":"\uA091",
                "J":"\uA02D",
                "K":"\uA017",
                "L":"\uA492",
                "M":"\uA052",
                "N":"\uA079",
                "O":"\uA182",
                "P":"\uA263",
                "Q":"\uA078",
                "R":"\uA493",
                "S":"\uA31A",
                "T":"\uA4C5",
                "U":"\uA407",
                "V":"\uA3DD",
                "W":"\uA150",
                "X":"\uA1D3",
                "Y":"\uA41F",
                "Z":"\uA074",
                }

special_cards = {
                "A":"\U0001F0C1",
                "B":"\U0001F0C8",
                "C":"\U0001F0CC",
                "D":"\U0001F0CD",
                "E":"\U0001F0C3",
                "F":"\U0001F0C6",
                "G":"\U0001F0C6",
                "H":"\U0001F0C1",
                "I":"\U0001F0CB",
                "J":"\U0001F0CB",
                "K":"\U0001F0CE",
                "L":"\U0001F0CB",
                "M":"\U0001F0CA",
                "N":"\U0001F0C4",
                "O":"\U0001F0CD",
                "P":"\U0001F0C9",
                "Q":"\U0001F0CD",
                "R":"\U0001F0C2",
                "S":"\U0001F0C5",
                "T":"\U0001F0C7",
                "U":"\U0001F0CB",
                "V":"\U0001F0CB",
                "W":"\U0001F0CA",
                "X":"\U0001F0CE",
                "Y":"\U0001F0C9",
                "Z":"\U0001F0C2",
                "a":"\U0001F0D1",
                "b":"\U0001F0D8",
                "c":"\U0001F0DC",
                "d":"\U0001F0DD",
                "e":"\U0001F0D3",
                "f":"\U0001F0D6",
                "g":"\U0001F0D6",
                "h":"\U0001F0D1",
                "i":"\U0001F0DB",
                "j":"\U0001F0DB",
                "k":"\U0001F0DE",
                "l":"\U0001F0DB",
                "m":"\U0001F0DA",
                "n":"\U0001F0D4",
                "o":"\U0001F0DD",
                "p":"\U0001F0D9",
                "q":"\U0001F0DD",
                "r":"\U0001F0D2",
                "s":"\U0001F0D5",
                "t":"\U0001F0D7",
                "u":"\U0001F0DB",
                "v":"\U0001F0DB",
                "w":"\U0001F0DA",
                "x":"\U0001F0DE",
                "y":"\U0001F0D9",
                "z":"\U0001F0D2",
                " ":"\U0001F0A0",
                }

special_upside_down = {
                "A":"\u2200",
                "C":"\u0186",
                "E":"\u018E",
                "F":"\u2132",
                "G":"\u05E4",
                "J":"\u017F",
                "L":"\u02E5",
                "M":"W",
                "P":"\u0500",
                "T":"\u2534",
                "U":"\u2229",
                "V":"\u039B",
                "W":"M",
                "Y":"\u2144",
                "a":"\u0250",
                "b":"q",
                "c":"\u0254",
                "d":"p",
                "e":"\u01DD",
                "f":"\u025F",
                "g":"\u0183",
                "h":"\u0265",
                "i":"\u1D09",
                "j":"\u027E",
                "k":"\u029E",
                "m":"\u026F",
                "n":"u",
                "p":"d",
                "q":"b",
                "r":"\u0279",
                "t":"\u0287",
                "u":"n",
                "v":"\u028C",
                "w":"\u028D",
                "y":"\u028E",
                "0":"0",
                "1":406,
                "2":4357,
                "3":400,
                "4":12579,
                "5":987,
                "6":"9",
                "7":12581,
                "8":"8",
                "9":"6",
                }

special_fullwidth = {
                "!": 65281,
                '"': 65282,
                "#": 65283,
                "$": 65284,
                "%": 65285,
                "&": 65286,
                "'": 65287,
                "(": 65288,
                ")": 65289,
                "*": 65290,
                "+": 65291,
                ",": 65292,
                "-": 65293,
                ".": 65294,
                "/": 65295,
                ":": 65306,
                ";": 65307,
                "<": 65308,
                "=": 65309,
                ">": 65310,
                "?": 65311,
                "@": 65312,
                "[": 65339,
                "\\": 65340,
                "]": 65341,
                "^": 65342,
                "_": 65343,
                "{": 65371,
                "|": 65372,
                "}": 65373,
                "~": 65374,
        }

special_pseudo_cyrillic = {
                "A": 1044,
                "B": 1042,
                "C": 1057,
                "D": 1070,
                "E": 1047,
                "F": 1170,
                "G": 1041,
                "H": 1290,
                "I": 1031,
                "J": 1032,
                "K": 1036,
                "L": 1027,
                "M": 1130,
                "N": 1048,
                "O": 1138,
                "P": 1056,
                "Q": 1060,
                "R": 1071,
                "S": 1029,
                "T": 1294,
                "U": 1039,
                "V": 1140,
                "W": 1064,
                "X": 1046,
                "Y": 1063,
                "Z": 1134,
        }

special_braille = {
    'a': '\u2801',
    'b': '\u2803',
    'c': '\u2809',
    'd': '\u2819',
    'e': '\u2811',
    'f': '\u280B',
    'g': '\u281B',
    'h': '\u2813',
    'i': '\u280A',
    'j': '\u281A',
    'k': '\u2805',
    'l': '\u2807',
    'm': '\u280D',
    'n': '\u281D',
    'o': '\u2815',
    'p': '\u280F',
    'q': '\u281F',
    'r': '\u2817',
    's': '\u280E',
    't': '\u281E',
    'u': '\u2825',
    'v': '\u2827',
    'w': '\u283A',
    'x': '\u282D',
    'y': '\u283D',
    'z': '\u2835',
    'A': '\u2820\u2801',
    'B': '\u2820\u2803',
    'C': '\u2820\u2809',
    'D': '\u2820\u2819',
    'E': '\u2820\u2811',
    'F': '\u2820\u280B',
    'G': '\u2820\u281B',
    'H': '\u2820\u2813',
    'I': '\u2820\u280A',
    'J': '\u2820\u281A',
    'K': '\u2820\u2805',
    'L': '\u2820\u2807',
    'M': '\u2820\u280D',
    'N': '\u2820\u281D',
    'O': '\u2820\u2815',
    'P': '\u2820\u280F',
    'Q': '\u2820\u281F',
    'R': '\u2820\u2817',
    'S': '\u2820\u280E',
    'T': '\u2820\u281E',
    'U': '\u2820\u2825',
    'V': '\u2820\u2827',
    'W': '\u2820\u283A',
    'X': '\u2820\u282D',
    'Y': '\u2820\u283D',
    'Z': '\u2820\u2835',
    '0': '\u283C\u281A',
    '1': '\u283C\u2801',
    '2': '\u283C\u2803',
    '3': '\u283C\u2809',
    '4': '\u283C\u2819',
    '5': '\u283C\u2811',
    '6': '\u283C\u280B',
    '7': '\u283C\u281B',
    '8': '\u283C\u2813',
    '9': '\u283C\u280A',
    "'": '\u2804',
    "/": '\u280C',
    ".": '\u2832',
    "!": '\u2816',
    ",": '\u2802',
    ";": '\u2806',
    "-": '\u2824',
    '"': '\u2826',
    "$": '\u2832',
    "?": '\u2826',
    "(": '\u2836',
    ")": '\u2836'
}

types = {
        "braille": char_set(special=special_braille),
        "circled": char_set(9398, 26, 9311, special_circled),
        "circled-negative": char_set(127312),
        "fullwidth": char_set(65313, 65345, 65296, special_fullwidth),
        "mathematical-bold": char_set(119808, 26, 120782),
        "mathematical-bold-fraktur": char_set(120172, 26),
        "mathematical-bold-italic": char_set(119912, 26),
        "mathematical-bold-script": char_set(120016, 26),
        "mathematical-italic": char_set(119860, 26),
        "mathematical-monospace": char_set(120432, 26, 120822),
        "mathematical-sans-serif": char_set(120224, 26, 120802),
        "mathematical-sans-serif-bold": char_set(120276, 26, 120812),
        "mathematical-sans-serif-bold-italic": char_set(120380, 26),
        "mathematical-sans-serif-italic": char_set(120328, 26),
        "parenthesized": char_set(127248, 9372, 9331, {"0": 48}),
        "playing-cards": char_set(special = special_cards, digit = 127136),
        "pseudo-cyrillic": caps_special_char_set(special_pseudo_cyrillic),
        "regional-indicator": char_set(127462),
        "runic": char_set(special = special_runic),
        "squared": char_set(127280),
        "squared-negative": char_set(127344),
        "upside-down": char_set(special = special_upside_down),
        "INCOMPLETE:mathematical-double-struck": char_set(120120, 26, 120792),
        "INCOMPLETE:mathematical-fraktur": char_set(120068, 26),
        "INCOMPLETE:mathematical-script": char_set(119964, 26),
        "upper": char_set(65, 65, special={"." : "!"}),
        "lower": char_set(97, 97, special={
            "!": ".", "?": "."}),
        }

def convert(lang, text, joinstring = ""):
    if lang in types.keys():
        return joinstring.join([types[lang](c) for c in text])

if __name__ == "__main__":
    
    t = sys.argv[1]
    startarg = 2
    joinstring = ""

    if len(sys.argv) < 2:
        print_help()
    elif sys.argv[1] == "--help":
        print_help()
    elif sys.argv[1] == "--langs":
        print_sets()
        quit()

    if len(sys.argv) < 3:
        print_help()
    elif sys.argv[2] == "--space":
        startarg = 3
        joinstring = " "

    if len(sys.argv) < startarg + 1:
        print_help()

    txt = " ".join(sys.argv[startarg:])

    if sys.argv[1] == "upside-down":
        txt = txt[::-1]

    uni = convert(t, txt, joinstring)
    print(uni)
