#!/usr/bin/env python3

import convert
import sys

NOTRANS = 0
TRANS = 1

def pc(c):
    print(c, end="")

def do(type : str, basefile : str, writefile : str, mark : str = "Generated"):
    attributes = {}
    comments = []
    if mark != "":
        comments.append("# " + mark + "\n")
    with open(basefile, "r", encoding="utf-8") as f:
        key = ""
        text = []
        noKeyInNextLine = False
        for l in f:
            if l.startswith("#"):
                comments.append(l)
            skip = 0
            if noKeyInNextLine:
                isKey = False
                noKeyInNextLine = False
            else:
                key = ""
                isKey = True
                text = []
            checkNext = False
            direct = 0
            for i, c in enumerate(l):
                if checkNext and not isKey:
                    if c == "\\":
                        text.append([TRANS, "\\"])
                    elif c == "%":
                        text.append([TRANS, "\\%"])
                    elif c == "n":
                        text.append([NOTRANS, "\\n"])
                    elif c == "u":
                        text.append([NOTRANS, "\\u"])
                        direct = 4
                    elif c == "\n":
                        text.append([NOTRANS, "\\\n"])
                        noKeyInNextLine = True
                    checkNext = False
                elif c == "%":
                    text.append([NOTRANS, "%"])
                    direct = 1
                elif direct > 0 and not isKey:
                    text[-1][1] += c
                    direct -= 1
                elif c == "=":
                    isKey = False
                elif c == "\\":
                    checkNext = True
                elif c == "\n":
                    pass
                else:
                    if isKey:
                        key += c
                    else:
                        text.append([TRANS, c])
            if noKeyInNextLine:
                pass
            else:
                attributes[key] = text

    if type in convert.types:
        pass
    else:
        from argostranslate import package, translate

    with open(writefile, "w", encoding="utf-8") as f:
        for l in comments:
            f.write(l)
        for key, text in attributes.items():
            writeText = key + "="
            if type == "upside-down":
                text.reverse()
            elif type == "blank":
                text = []
            for c in text:
                if c[0] == NOTRANS:
                    writeText += c[1]
                elif c[0] == TRANS:
                    writeText += convert.convert(type, c[1])
            f.write(writeText + "\n")
