#!/usr/bin/env python3.9
import sort
import translate

def path(filename : str):
    return "/home/q/IdeaProjects/swp2020a/client/src/main/resources/i18n/" + filename + ".properties"

base = path("SWP2020A")

sort.do(path("SWP2020A"))
sort.do(path("SWP2020A_de_DE_du"))
sort.do(path("SWP2020A_de_DE_sie"))
sort.do(path("SWP2020A_en_GB_improved"))
sort.do(path("SWP2020A_en_GB_UwU"))
sort.do(path("SWP2020A_nds_DE"))

translate.do("pseudo-cyrillic", base, path("SWP2020A_en_GB_pseudo-cyrillic"))
translate.do("playing-cards", base, path("SWP2020A_en_GB_playing-cards"))
translate.do("upside-down", base, path("SWP2020A_en_GB_upside-down"))
translate.do("braille", base, path("SWP2020A_en_GB_blind"))
translate.do("upper", base, path("SWP2020A_en_GB_hearing-impaired"))
translate.do("blank", base, path("SWP2020A_en_GB_blank"))
translate.do("lower", base, path("SWP2020A_en_GB_lowercase"))
translate.do("upper", path("SWP2020A_nds_DE"), path("SWP2020A_nds_DE_for-olle-luu"))
translate.do("upper", path("SWP2020A_en_GB_UwU"), path("SWP2020A_en_GB_uwu-scweam"))
translate.do("lower", path("SWP2020A_en_GB_UwU"), path("SWP2020A_en_GB_uwu-wowewcase"))
